import React, { Component } from 'react';
import { connect } from 'dva';
import { message, Spin } from 'antd';
import styles from './index.less';
import VideoPlayer from '../../components/VideoPlayer';

function tips(type, title) {
  message.destroy();
  message[type](title);
}

class IndexPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlList: [
        { url: 'rtmp://live.2410.daily.xinhuazhiyun.com/agent/74' },
        { url: 'rtmp://live.hkstv.hk.lxdns.com/live/hks1' },
        { url: 'rtmp://live.2410.daily.xinhuazhiyun.com/agent/17' },
        { url: 'rtmp://mobliestream.c3tv.com:554/live/goodtv.sdp' },
        { url: 'rtmp://58.200.131.2:1935/livetv/hunantv' },
        { url: 'rtmp://media3.sinovision.net:1935/live/livestream' },
        {
          url:
            'http://live-daily.xinhuazhiyun.com/caster/457f301dd2034770b2c92b8506df9aab.flv?auth_key=1562747264-0-0-74836aa58ea569de81cf5b58ece8843a',
        },
      ],
    };
  }

  componentDidMount() {
    // this.props.dispatch({
    //   type: 'main/getVideo',
    //   payload: {},
    //   onResult: (data) => {
    //     this.setState({
    //       url: data
    //     })
    //   }
    // })
  }

  componentWillUnmount() { }

  render() {
    const { urlList } = this.state;

    return (
      <div className={styles.main}>

        {urlList &&
          urlList.map((item, index) => {
            return (
              <div key={index} className={styles.jkVideo}>
                <VideoPlayer
                  videoJsOptions={{
                    // autoplay: true,
                    // controls: true,
                    // language: 'zh-CN',
                    errorDisplay: false,
                    preload: 'auto',
                    width: 470,
                    height: 265,
                    bigPlayButton: false,
                    textTrackDisplay: false,
                    posterImage: false,
                    controlBar: false,
                    textTrackSettings: false,
                    // loadingSpinner: false,
                    userActions: {
                      hotkeys: true,
                    },
                    sources: {
                      src: item.url,
                    },
                  }}
                />
              </div>
            );
          })}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

export default connect(mapStateToProps)(IndexPage);
