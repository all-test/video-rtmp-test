import React, { Component } from 'react';
import { connect } from 'dva';
// import { message } from 'antd';
// import styles from './index.less';

// function tips(type, title) {
//   message.destroy();
//   message[type](title);
// }

class AliPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }


  componentDidMount() {
    const player = new Aliplayer(
      {
        id: 'player-con',
        // source: 'rtmp://live.hkstv.hk.lxdns.com/live/hks1',
        // source: 'rtmp://live.2410.daily.xinhuazhiyun.com/agent/151',
        source:
          'http://live-daily.xinhuazhiyun.com/caster/e95820e054c54c09b69f70f7bd8dc443.flv?auth_key=1562401664-0-0-871437f43bf8984a62230ceabfafadce',
        width: '100%',
        height: '500px',
        autoplay: true,
        isLive: true,
        rePlay: false,
        playsinline: true,
        preload: true,
        language: 'zh-cn',
        controlBarVisibility: 'hover',
        useH5Prism: true,
        skinLayout: false,
        extraInfo: {
          liveStartTime: '2019/08/17 12:00:00',
        },
      },
      (() => {
        console.log('The player is created');
      }),
    );
  }
  render() {
    return <div className="prism-player" id="player-con" />;
  }
}

// const mapStateToProps = (state) => {
//   return {};
// };

export default connect()(AliPlayer);
