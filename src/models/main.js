import * as mainServices from '../services/main';
export default {

  namespace: 'main',

  state: {
    list: []
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line

    }
  },

  effects: {
    *getVideo({ payload, onResult }, { call, put }) {  // eslint-disable-line
      const result = yield call(mainServices.getVideo)
      if (result) {
        onResult && onResult(result.msg)
      }
    }
  },

  reducers: {
    save(state, action) {
      return {...state, ...action.payload};
    }
  }

};
