import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { message, Icon, Spin } from 'antd';
import styles from './index.less';
import videojs from 'video.js';
import 'video.js/dist/video-js.css';
import 'videojs-flash';

function tips(type, title) {
  message.destroy();
  message[type](title);
}

class VideoPlayer extends PureComponent {
  constructor(props) {
    super(props);
    this.player;
    this.state = {
      isPlay: false,
      isloading: false,
    };
  }

  componentDidMount() {
    this.player = videojs(this.videoNode, this.props.videoJsOptions);
    // this.player.on('loadeddata', () => {
    // 	this.setState({
    // 		isloading: true
    // 	})
    // })

    // this.player.on('timeupdate', () => {
    // 	this.setState({
    // 		isloading: false,
    // 	})
    // })
  }

  componentWillUnmount() {
    if (this.player) {
      this.player.dispose();
    }
  }

  play = () => {
    const { isPlay } = this.state;
    this.setState({
      isPlay: !isPlay,
    });
    if (isPlay) {
      this.player.pause();
    } else {
      // this.setState({
      // 	isloading: true
      // })
      this.player.play();
    }
  };

  render() {
    const { isPlay, isloading } = this.state;
    return (
      <div className={styles.videoMain}>
        {/* <Spin spinning={isloading} indicator={<Icon type="loading-3-quarters" spin style={{ fontSize: 36 }}/>}> */}
        <video ref={(node) => (this.videoNode = node)} className="video-js" />
        <div className={styles.controls}>
          <div className={styles.ppBtn} onClick={this.play}>
            <Icon type={isPlay ? 'pause-circle' : 'play-circle'} theme="filled" />
          </div>
        </div>
        {/* </Spin> */}
      </div>
    );
  }
}

export default connect()(VideoPlayer);
