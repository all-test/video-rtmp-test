import React, { Component, Children } from 'react';
import { connect } from 'dva';
import { message, Layout } from 'antd';
import styles from './index.less';

const { Header, Content } = Layout;

class MLayout extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<Layout>
				<Header />
				<Content>{this.props.children}</Content>
			</Layout>
		);
	}
}

export default connect()(MLayout);
